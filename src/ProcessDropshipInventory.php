<?php

namespace RiteAid\InventorySubscriber;

use League\Flysystem\FilesystemInterface;
use Magement\Subscriber\SubscriberInterface;
use Magement\Subscriber\SubscriberOutputInterface;

class ProcessDropshipInventory implements SubscriberInterface
{
    const COMPONENT_TYPE = 'file';
    const FILE_EXTENSION = 'csv';
    const STATUS_SUCCESS = '1';
    const STATUS_ERROR = '0';
    const RECEIVED_FILE_LOCATION = 'received';


    /**
     * @var FilesystemInterface
     */
    private $fileSystem;

    /**
     * @var string
     */
    private $sourceId;

    /**
     * @var string
     */
    private $mode;

    /**
     * @var array|mixed
     */
    private $configuration;

    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * Constructor.
     * @param FilesystemInterface $fileSystem
     * @param string $sourceId
     * @param string $mode
     */
    public function __construct(
        FilesystemInterface $fileSystem,
        string $sourceId,
        string $mode
    ) {
        $this->fileSystem = $fileSystem;
        $this->sourceId = $sourceId;
        $this->mode = $mode;
        $this->configuration = $this->getApiConfiguration();
        $this->client = $this->getClient();
    }

    /**
     * @return array
     */
    private function getApiConfiguration()
    {
        //TODO: fetch confirmation from environment variable
        $configuration = [
            'mom_api_token_url' => 'TOKEN',
            'client_id' => 'CLIENT ID',
            'client_secret' => 'SECRET KEY'
        ];

        return $configuration;
    }

    /**
     * This function is used to get GuzzleHttp client
     *
     * @return \GuzzleHttp\Client
     */
    private function getClient() {
        return new \GuzzleHttp\Client();
    }

    /**
     * @param SubscriberOutputInterface $output
     * @return void
     */
    public function execute(SubscriberOutputInterface $output): void
    {
        try {
            $files = $this->fileSystem->listContents();

            foreach ($files as $file) {
                if ($file['type'] == self::COMPONENT_TYPE) {
                    $pathInfo = pathinfo($file['path']);
                    if ($pathInfo['extension'] == self::FILE_EXTENSION) {
                        try {
                            $fileContent = $this->fileSystem->read($file['path']);
                            $apiReponse = $this->updateMOMInventory($fileContent);

                            if ($apiReponse['success']) {
                                $this->moveFileToReceived($file['path'], $fileContent);
                            }
                        } catch (\Exception $e) {
                            /*
                            * TODO: Need to add logger class
                            */
                            echo $e->getMessage();
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            /*
            * TODO: Need to add logger class
            */
            echo $e->getMessage();
        }
    }

    /**
     * This function is used to move file to received directory
     *
     * @param $filePath
     * @param $fileContent
     * @return void
     */
    private function moveFileToReceived($filePath, $fileContent)
    {
        try {
            $this->fileSystem->write('/' . self::RECEIVED_FILE_LOCATION . '/' . $filePath, $fileContent);
            $this->fileSystem->delete($filePath);
        } catch (\Exception $e) {
            /*
             * TODO: Need to add logger class
             */
            echo $e->getMessage();
        }
    }

    /**This function is used to update MOM Inventory
     *
     *
     * @param $fileContent
     * @return array
     */
    private function updateMOMInventory($fileContent)
    {
        $inventoryUpdateJson = $this->buildJsonForMOMInventoryUpdate($fileContent);

        $requestBody =
            [
                'jsonrpc' => '2.0',
                'id' => 1,
                'method' => 'magento.inventory.source_stock_management.update',
                'params' => $inventoryUpdateJson
            ];

        $apiResponse = [];
        try {
            $completeRequest = [
                'api_endpoint' => $endpoint,
                'header_and_request' =>
                    [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $this->authenticationToken,
                            'Content-Type' => 'application/json',
                        ],
                        'json' => $requestBody
                    ]
            ];
            $response = $this->client->request(
                'POST',
                $completeRequest['api_endpoint'],
                $completeRequest['header_and_request']
            );

            $body = $response->getBody();

            if (!empty($body)) {

                // Explicitly cast the body to a string
                $stringBody = (string)$body;
                $apiResponse = json_decode($stringBody, true);

                if (!isset($apiResponse['error'])) {
                    $this->logManager->debugLog(
                        $requestBody['method'] . ' API Request And Response',
                        $completeRequest,
                        $apiResponse
                    );
                    return [
                        'success' => self::STATUS_SUCCESS,
                        'response' => $apiResponse
                    ];
                } else {
                    throw new Exception(
                        $requestBody['method'] . ' There was an error processing your request.'
                    );
                }
            } else {
                throw new Exception($requestBody['method'] . ' CURL request received empty response.');
            }
        } catch (Exception $e) {
            return [
                'success' => self::STATUS_ERROR,
                'message' => $e->getMessage()
            ];
        }
    }

    /**
     * This function is used to build json for MOM Inventory API
     *
     * @param $fileContent
     * @return array
     */
    private function buildJsonForMOMInventoryUpdate($fileContent)
    {
        $fileLines = explode(PHP_EOL, $fileContent);

        $i = 0;
        $inventoryInfo = [];
        $inventoryUpdateJson = [];
        foreach ($fileLines as $fileLine) {
            $i++;
            if ($i == 1) {
                continue;
            }
            if ($fileLine == '') {
                continue;
            }

            list($vendorSku, $qty) = explode(',', $fileLine);
            $inventoryInfo[] = ['quantity' => $qty, 'sku' => $vendorSku];

            $i++;
        }

        $inventoryUpdateJson['snapshot'] = [
            'created_on' => date('c'),
            'mode' => $this->mode,
            'source_id' => $this->sourceId,
            'stock' => $inventoryInfo
        ];

        return $inventoryUpdateJson;
    }
}

